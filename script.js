
let number1 = prompt("Enter first number");

let number2 = prompt("Enter second number");
let operatorInput = prompt("Enter math operator");

while(isNaN(number1) || isNaN(number2) || (number1 === "") || (number2 === "")  || number2 === null || number1 === null){
    number1 = prompt("Enter first number", number1);
    number2 = prompt("Enter second number", number2);
}

function countMathOperation(firstNum, secondNum, operatorInput) {
    firstNum = +firstNum;
    secondNum = +secondNum;
    switch (operatorInput) {
        case "+":
            return (firstNum + secondNum);
            break;
        case "-":
            return (firstNum - secondNum);
            break;
        case "*":
            return (firstNum * secondNum);
            break;
        case "/":
            return (firstNum / secondNum);
            break;
    }
}
console.log(countMathOperation(number1, number2, operatorInput));

//Теоретическая часть
//Функции нужны, чтобы избежать повторения в коде, для однорозовой записи алгоритма
//Аргументы функции нужны, чтобы была возможность оперировать внешними данными,
// и чтобы была возможность подставлять каждый раз новые внешние данные
